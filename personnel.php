<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
       <title>AIR FORCE BASE TAKORADI</title>
    <link rel="icon" href="images/airforce.png">

   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="Css/personnel.css">
   <link href="Css/font-awesome.css" rel="stylesheet">
   <!-- //font-awesome-icons -->
   <!-- /Fonts -->
   <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700" rel="stylesheet">
   <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

    <!-- //Fonts -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/
    3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/
    respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <?php require_once 'connection.php';?>
      <h1 id="person">PERSONNEL INFORMATION</h1>
      <?php
      if(isset($_SESSION['message'])):?>
      <div class="alert alert-<?=$_SESSION['msg_type']?>">
        <?php
          echo $_SESSION['message'];
          unset($_SESSION['message']);
          ?>
      </div>
    <?php endif ?>
      <div class="container">
      <?php
      $connect = new mysqli('127.0.0.1', 'root', '', 'personnel') or die(mysqli_error($connect));
      $result = $connect->query("SELECT * FROM afbperson") or die($connect->error);
      ?>
      <div class="row justify-content-cente">
      <table class="table">
      <thead>
      <tr>
      <th>SRL NO</th>
      <th>SVC NO</th>
      <th>RANK</th>
      <th>NAME</th>
      <th>SECTION</th>
      <th>TRADE</th>
      <th>REMARKS</th>
      <th colspan="1">Action</th>
      </tr>
      </thead>
      <?php
      while($row = $result->fetch_assoc()):?>
      <tr>
         <td><?php echo $row['SRL'];?></td>
         <td><?php echo $row['SVC'];?></td>
         <td><?php echo $row['RANK'];?></td>
         <td><?php echo $row['NAME'];?></td>
         <td><?php echo $row['SECTION'];?></td>
         <td><?php echo $row['TRADE'];?></td>
         <td><?php echo $row['REMARKS'];?></td>
         <td>
           <a href="personnel.php?edit=<?php echo $row['SRL'];?>"
           class="btn btn-info">Edit</a>
           <a href="connection.php?delete=<?php echo $row['SRL'];?>"
           class="btn btn-danger">Delete</a>
        </td>

      </tr>
    <?php endwhile; ?>
      </table>
      </div>
      <?php
      function pre_r( $array){
      echo '<pre>';
      print_r($array);
      echo '</pre>';
      }
      ?>










      <form action="connection.php" method="POST">
        <div class="form-row">
          <div class="col-md-4 mb-3">
            <label for="validationDefault01">SRL</label>
            <input type="text" class="form-control" id="validationDefault01" value="<?php echo $srl; ?>" placeholder="ENTER YOUR SRL" name="SRL" required>
          </div>
          <div class="col-md-4 mb-3">
            <label for="validationDefault02">SVC NO</label>
            <input type="text" class="form-control" id="validationDefault02" value="<?php echo $svc; ?>" placeholder="ENTER SVC NO" name="SVC" required>
          </div>
        <div class="form-row">
          <div class="col-md-6 mb-3">
            <label for="validationDefault03">RANK</label>
            <input type="text" class="form-control" id="validationDefault03" value="<?php echo $rank; ?>" placeholder="ENTER YOUR RANK"  name="RANK" required>
          </div>
          <div class="form-row">
            <div class="col-md-6 mb-3">
              <label for="validationDefault03">NAME</label>
              <input type="text" class="form-control" id="validationDefault03" value="<?php echo $name; ?>" placeholder="ENTER YOUR FULL NAME" name="NAME" required>
            </div>
            <div class="form-row">
                <div class="col-md-6 mb-3">
                  <label for="validationDefault03">SECTION</label>
                  <input type="text" class="form-control" id="validationDefault03" value="<?php echo $section; ?>" placeholder="ENTER YOUR SECTION" name="SECTION"required>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                      <label for="validationDefault03">TRADE</label>
                      <input type="text" class="form-control" id="validationDefault03" value="<?php echo $srl; ?>" placeholder="ENTER YOUR TRADE" name="TRADE"required>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                          <label for="validationDefault03">SECTION</label>
                          <input type="text" class="form-control" id="validationDefault03" placeholder="ENTER THE REMARKS" name="REMARKS"required>
                        </div>

        <button class="btn btn-primary" type="submit" name="Submit">Submit</button>
      </form>
    </div>
